﻿# lesi
A standalone **L**ua **E**mbedded **S**cript **I**nterpreter

## About

 1. Converts target .lua scripts to C resource files
 2. Compiles a modified PUC-Rio Lua with the resource loader to find embedded scripts
 3. Generates a freestanding executable that has embedded Lua scripts in it.
 
 ![Screenshot](https://www.cloverbasin.com/screenshots/lesi_screenshot.png)
 
## Requirements
 
1. Compiler toolchain like Visual Studio Tools / MSYS2 MinGW / Linux
2. Cmake
3. Lua 5.1, 5.2, 5.3, 5.4, (luajit not supported yet)
4. Build tool supported by cmake: ninja, mingw32-make, gnu make

## Usage
 1. Add .lua scripts to lesi/scripts directory.
 2. Edit lesi/config/scripts_to_embed.cmake and include the names of the scripts to embed.
 3. Update lesi/config/lua_hardcoded.cmake with paths to target Lua installation.
 4. Execute a build

## Build
from your compiler command prompt (For example: MSYS2 UCRT64)
 1. cd lesi/
 2. mkdir build
 3. cd build
 4. cmake -G "MinGW Makefiles" -DCMAKE_BUILD_TYPE=Release  ..
 5. mingw32-make install
 


