set (LUA_TARGET_VERSION 501)

### Full path to lua command line interpreter.
set(LUA_BINARY "u:/msys64/ucrt64/bin/lua5.1.exe")

### Full path to lua.h, lua.hpp, lauxlib.h, luaconf.h, lualib.h
set(LUA_INCLUDE "u:/msys64/ucrt64/include/lua5.1")

### Full path to lua library.
#### Can be 1 of 3 options:
#### 1) Full path to the static library (liblua.a)
#### 2) Full path to the shared library (lua.dll/lua.so)
#### 3) Full path to the import library (liblua.dll.a) (Windows Specific)
if(USE_STATIC_LUA)
    set(LUA_LIBRARY "u:/msys64/ucrt64/lib/liblua5.1.a")
else()
    #set(LUA_LIBRARY "u:/msys64/ucrt64/bin/lua54.dll")
    set(LUA_LIBRARY "u:/msys64/ucrt64/lib/liblua5.1.dll.a")
endif()
