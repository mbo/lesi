set(SCRIPT_SOURCES main.lua helloworld.lua)

# lua-socket .lua sources.
set(SOCKET_SOURCES ltn12.lua mbox.lua mime.lua socket.lua socket_ftp.lua socket_headers.lua socket_http.lua socket_smtp.lua socket_tp.lua socket_url.lua)
list(APPEND SCRIPT_SOURCES ${SOCKET_SOURCES})
