local hw = assert(require("helloworld"))

print("Start: main.")
hw.hw()
local arg = { select(1, ...) }
for i, v in ipairs(arg) do print(i, v) end
print("main done.")
