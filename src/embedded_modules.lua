local embedded_header = [[
#include <generated_modules.h>

]]

local struct_header = [[
struct generated_module generated_modules[] =
{
]]

local generated_file = select(1, ...) .. "/generated_modules.c"
local module_names = { select(2, ...) }

local outf = io.open(generated_file, "w")
if outf then
    outf:write(embedded_header)
    for i = 1, #module_names do
        local module_name, loader = module_names[i]:match("^([^/]+)/(.+)$")
        outf:write(string.format('int %s(lua_State* L);\n', loader))
    end
    outf:write("\n",struct_header)
    
    for i = 1, #module_names do
        local module_name, loader = module_names[i]:match("^([^/]+)/(.+)$")
        outf:write(string.format('\t{ "%s", %s },\n', module_name, loader))
    end
    
    outf:write("\t{ NULL, NULL}\n};\n")
    outf:close()
else
    print("couldn't open: ", generated_file)
    return 1
end

return 0
