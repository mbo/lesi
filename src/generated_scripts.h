#ifndef GENERATED_SCRIPTS_H
#define GENERATED_SCRIPTS_H

#include <stddef.h>

struct generated_script
{
    const char *name;
    const char *data;
    const size_t data_size;
};

extern const char script_main[];
extern const size_t script_main_size;

extern struct generated_script generated_scripts[];

#endif // GENERATED_SCRIPTS_H
