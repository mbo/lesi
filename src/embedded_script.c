#include <string.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include "embedded_script.h"
#include "generated_scripts.h"
#include "generated_modules.h"

int embedded_script_searcher(lua_State *L)
{
    int status = 0;
    const char* require_name = luaL_checkstring(L, 1);
    const char* module_name = luaL_gsub(L, require_name, ".", "_");
    
    for (size_t i = 0; generated_scripts[i].name != NULL; ++i)
    {
        struct generated_script *gs = &generated_scripts[i];
        if (strcmp(module_name, gs->name) == 0)
        {
            int rv = luaL_loadbuffer(L, gs->data, gs->data_size, gs->name);
            if (rv == LUA_OK)
            {
                lua_pushvalue(L, 1);
                status = 2;
            }
        }
    }
    
    return status;
}

int embedded_module_searcher(lua_State *L)
{
    int status = 0;
    const char* require_name = luaL_checkstring(L, 1);
    const char* module_name = luaL_gsub(L, require_name, ".", "_");
    
    for (size_t i = 0; generated_modules[i].name != NULL; ++i)
    {
        struct generated_module *gm = &generated_modules[i];
        if (strcmp(module_name, gm->name) == 0)
        {
            lua_pushcfunction(L, gm->loader);
            lua_pushvalue(L, 1);
            status = 2;
        }
    }
    
    return status;
}

int embedded_script_enable_searcher(lua_State* L)
{
    int status = 0;
    int top = lua_gettop(L);
    
    // Lua equivalent
    // package.searchers[#package.searchers + 1] = embedded_script_searcher
    // putting this at the end of the searchers table enables overriding the embedded scripts of the same name.
    lua_getglobal(L, "package");
    if (lua_type(L, -1) == LUA_TTABLE)
    {
        lua_getfield(L, -1, PACKAGE_SEARCHER);
        if (lua_type(L, -1) == LUA_TTABLE)
        {
            lua_len(L, -1);
            lua_Integer searcher_count = lua_tointeger(L, -1);
            lua_pop(L, 1);
            
            lua_pushcfunction(L, embedded_script_searcher);
            lua_rawseti(L, -2,  searcher_count + 1);
            lua_pushcfunction(L, embedded_module_searcher);
            lua_rawseti(L, -2,  searcher_count + 2);
            
            status = 1;
        }
    }
    // function not called by Lua VM, clean up our own stack.
    lua_settop(L, top);
    return status;
}

int embedded_script_insert_script(lua_State *L, char ***argv, int *argc, int first_arg)
{
    char **oldv = *argv;
    char **margv = NULL;
    
    // Lua 5.3 leaves "no script" index at the end of the array.
    // set value to 0 to make it behave like 5.4.
    if (first_arg == *argc) { first_arg = 0; }
    // skip -1, and '-' conditions.
    else if (first_arg < 0) { return 0; }
    else if (strcmp(oldv[first_arg], "-") == 0) { return 0; }
    // allocate a slot for our script_main string, and also NULL terminator.
    margv = lua_newuserdata(L, (*argc + 2) * sizeof *margv);
    lua_setfield(L, LUA_REGISTRYINDEX, "script_main_argv");
    // NULL terminate argv for Lua 5.1.
    margv[*argc + 1] = NULL;
    
    // no script, insert script at the end.
    if (first_arg == 0)
    {
        for (int i = 0; i < *argc; ++i) { margv[i] = oldv[i]; }
        margv[*argc] = "script_main";
    }
    // insert script at before arguments lesi.
    else
    {
        int offset = 0;
        for (int i = 0; i < *argc; ++i)
        {
            if (i == first_arg) { margv[i + offset] = "script_main"; offset = 1; }
            margv[i + offset] = oldv[i];
        }
    }
    
    *argv = margv;
    ++(*argc);
    
    return *argv != oldv;
}

int embedded_script_load_main(lua_State *L)
{
    return luaL_loadbuffer(L, script_main, script_main_size, "main");
}
