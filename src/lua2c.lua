function lua2c(base_name, destination_file, file_contents)
    local result
    local fd = io.open(destination_file, "w")
    if fd then
        fd:write(string.format("#define SCRIPT_%s_SZ %u\n", base_name:upper(), #file_contents))
        fd:write(string.format("const size_t script_%s_size = %u;\n", base_name, #file_contents))
        fd:write(string.format("const char script_%s[%u] = {\n", base_name, #file_contents))
        fd:write("\t")
        
        for i = 1, #file_contents do
            fd:write(string.format("0x%02x", string.byte(file_contents, i)))
            if i ~= #file_contents then fd:write(", ") end
            if math.fmod(i, 16) == 0 then fd:write("\n\t") end
        end
        
        fd:write("\n};")
        fd:close()
        result = true
    end
    
    return result
end

function get_file_contents(filename)
    local file_contents
    
    local fd = io.open(filename, "r")
    if fd then
        file_contents = fd:read("*a")
        fd:close()
    end
    
    return file_contents
end

function main(filename, source, destination)
    local exit_code = 1
    
    local base_name = filename:gsub("%.lua$", "")
    local destination_file = string.format("%s/script_%s.c", destination, base_name)
    local file_contents = get_file_contents(string.format("%s/%s", source, filename))
    
    if file_contents and lua2c(base_name, destination_file, file_contents) then
        exit_code = 0
    end
end

local filename, source, destination = assert(select(1, ...))
os.exit(main(filename, source, destination))
