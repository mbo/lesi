#ifndef GENERATED_MODULES_H
#define GENERATED_MODULES_H

#include <lua.h>

struct generated_module
{
    const char *name;
    lua_CFunction loader;
};

extern struct generated_module generated_modules[];

#endif // GENERATED_MODULES_H
