#include <windows.h>
#include <stdlib.h> /* declaration of __argc and __argv */

extern int main(int, char **);

int PASCAL WinMain(HINSTANCE hinst, HINSTANCE hprev, LPSTR cmdline, int ncmdshow)
{
  int rc;
  
  extern int __argc;     /* this seems to work for all the compilers we tested, except Watcom compilers */
  extern char** __argv;
  
  rc = main(__argc, __argv);
  
  return rc;
}

/* Copied from: https://github.com/LuaDist/srlua/blob/master/wmain.c, all other files are considered public domain.
* srlua.c
* Lua interpreter for self-running programs
* Luiz Henrique de Figueiredo <lhf@tecgraf.puc-rio.br>
* 27 Apr 2012 09:24:34
* This code is hereby placed in the public domain.
*/
