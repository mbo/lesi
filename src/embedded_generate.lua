local embedded_header = [[
#include <generated_scripts.h>
]]

local struct_header = [[
struct generated_script generated_scripts[] =
{
]]

local generated_file = select(1, ...) .. "/generated_scripts.c"
local script_names = { select(2, ...) }

local outf = io.open(generated_file, "w")
if outf then
    outf:write(embedded_header)
    
    for i = 1, #script_names do
        outf:write(string.format('#include "script_%s.c"\n', script_names[i]))
    end
    
    outf:write("\n",struct_header)
    
    for i = 1, #script_names do
        if script_names[i] ~= "main" then
            outf:write(string.format('\t{ "%s", script_%s, SCRIPT_%s_SZ },\n', script_names[i], script_names[i], script_names[i]:upper()))
        end
    end
    
    outf:write("\t{ NULL, NULL, 0 }\n};\n")
    
    outf:close()
else
    print("couldn't open: ", generated_file)
    return 1
end

return 0
