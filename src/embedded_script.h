#ifndef EMBEDDED_SCRIPT_H
#define EMBEDDED_SCRIPT_H

#include <lua.h>

#ifndef LUA_OK
#define LUA_OK 0
#endif

#if LUA_VERSION_NUM < 502
    #define PACKAGE_SEARCHER "loaders"
#else
    #define PACKAGE_SEARCHER "searchers"
#endif

#if LUA_VERSION_NUM < 502
#define lua_len(L, idx) do { \
    lua_pushinteger(L, (lua_Integer) lua_objlen(L, idx)); \
} while (0)
#endif

int embedded_script_enable_searcher(lua_State *L);
int embedded_script_load_main(lua_State *L);
int embedded_script_insert_script(lua_State *L, char ***argv, int *argc, int first_arg);

#endif
