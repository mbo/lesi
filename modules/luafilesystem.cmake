list(APPEND EMBEDDED_MODULE_TARGETS lua-filesystem)
list(APPEND EMBEDDED_MODULE_NAMES "lfs/luaopen_lfs")

set(LFS_SRC lfs-912e067/lfs.c)
include_directories(BEFORE ${LUA_INCLUDE})

add_library(lua-filesystem STATIC ${LFS_SRC})
target_link_libraries(lua-filesystem ${LUA_LIBRARY})
