list(APPEND EMBEDDED_MODULE_TARGETS lua-socket)
list(APPEND EMBEDDED_MODULE_NAMES "socket_core/luaopen_socket_core" "mime_core/luaopen_mime_core")

set(SOCKET_BASE_SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/luasocket-3.1.0)
set(SOCKET_BASE_SRC
    auxiliar.c
    buffer.c
    compat.c
    compat.h
    except.c
    inet.c
    io.c
    luasocket.c
    mime.c
    mime.h
    options.c
    select.c
    tcp.c
    timeout.c
    udp.c
)

if(NOT WIN32)
    list(APPEND EMBEDDED_MODULE_NAMES "socket_serial/luaopen_socket_serial" "socket_unix/luaopen_socket_unix")
    list(APPEND SOCKET_BASE_SRC unix.c unixdgram.c unixstream.c usocket.c serial.c)
    set(PLATFORM_LIBS "")
else()
    list(APPEND SOCKET_BASE_SRC wsocket.c)
    set(PLATFORM_LIBS WS2_32)
endif()

foreach(SOCKET_SRC_FILE IN LISTS SOCKET_BASE_SRC)
    list(APPEND SOCKET_SRC ${SOCKET_BASE_SRC_DIR}/${SOCKET_SRC_FILE})
endforeach()

include_directories(BEFORE ${LUA_INCLUDE})

add_library(lua-socket STATIC ${SOCKET_SRC})
target_link_libraries(lua-socket ${LUA_LIBRARY} ${PLATFORM_LIBS})
